#!/bin/bash

./simulation

echo "set terminal canvas size 1024,768 rounded standalone mousing jsdir \"http://gnuplot.sourceforge.net/demo_canvas\"; set output 'simulation.html'; plot [0:10] [-0.1:0.6] 'simulation.data' smooth unique with linespoints" > simulation.plot_html
echo "set terminal png size 1024,768; set output 'simulation.png'; plot [0:10] [-0.1:0.6] 'simulation.data' smooth unique with linespoints" > simulation.plot

gnuplot simulation.plot
gnuplot simulation.plot_html
