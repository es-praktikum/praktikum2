This is a sample SystemC Project using the current SystemC Github Repo and CMake.

Prerequisites: Git, CMake, C++ - Compiler (GCC, clang, Visual Studio Compiler (even only the Visual Studio Build Tools are enough).

steps:

```
git clone --recurse-submodules git://url.com
cd project && mkdir build
cmake ../
cmake --build .
```


For more sophisticated projects you can use Visual Studio Code with C++ and CMake plugin.