#pragma once

#define SC_INCLUDE_FX
#include <systemc.h>

#include "ALU.hpp"
#include "MUL.hpp"
#include "Multiplexer.hpp"
#include "Register.hpp"

template <class T>
class EquationSolver : public sc_module {
  public:
    sc_in<T> x0_in;
    sc_in<T> u0_in;
    sc_in<T> y0_in;
    sc_in<T> dx_in;
    sc_in<T> a_in;
    sc_out<T> y_out;

    sc_in<bool> clock;
    sc_in<bool> start;
    sc_out<bool> ready;

  private:
    /*****************************************
     * Alloziierte Komponenten instantiieren *
     *****************************************/

    MUL<T> m1;
    ...;

    ALU<T> a1;
    ...;

    Register<bool> rc;
    Register<T> rx;
    Register<T> rxneu;
    Register<T> ry;
    Register<T> ru;
    Register<T> r1;
    Register<T> r2;
    ...;

    Multiplexer4<T> m1_in1;
    Multiplexer4<T> m1_in2;
    Multiplexer2<T> m2_in1;
    Multiplexer2<T> m2_in2;
    Multiplexer4<T> a1_in1;
    Multiplexer4<T> a1_in2;

    Multiplexer2<T> rx_d;
    Multiplexer2<T> ry_d;
    Multiplexer2<T> ru_d;

    /*****************************************
     *         Signale instantiieren         *
     *****************************************/
    sc_signal<T> s_m1_in1;
    sc_signal<T> s_m1_in2;
    sc_signal<T> s_m1_out;

    sc_signal<T> s_m2_in1;
    sc_signal<T> s_m2_in2;
    sc_signal<T> s_m2_out;

    sc_signal<T> s_a1_in1;
    sc_signal<T> s_a1_in2;
    sc_signal<T> s_a1_out;
    sc_signal<bool> s_a1_leq;
    sc_signal<sc_uint<2> > s_a1_op;

    // s_a2_in1 ist s_ry_q
    // s_a2_in2 ist s_r7_q
    sc_signal<T> s_a2_out;
    sc_signal<bool> s_a2_leq;
    sc_signal<sc_uint<2> > s_a2_op;

    sc_signal<T> s_rx_d;
    sc_signal<T> s_rx_q;
    sc_signal<bool> s_rx_write;

    // sc_signal<T> s_rxneu_d; ist A1_out
    sc_signal<T> s_rxneu_q;
    sc_signal<bool> s_rxneu_write;

    sc_signal<T> s_ry_d;
    sc_signal<T> s_ry_q;
    sc_signal<bool> s_ry_write;

    sc_signal<T> s_ru_d;
    sc_signal<T> s_ru_q;
    sc_signal<bool> s_ru_write;

    sc_signal<bool> s_rc_d;
    sc_signal<bool> s_rc_q;
    sc_signal<bool> s_rc_write;

    // s_r1_d ist s_m1_out
    sc_signal<T> s_r1_q;
    sc_signal<bool> s_r1_write;

    // s_r2_d ist s_m2_out
    sc_signal<T> s_r2_q;
    sc_signal<bool> s_r2_write;

    // s_r3_d ist s_m1_out
    sc_signal<T> s_r3_q;
    sc_signal<bool> s_r3_write;

    // s_r4_d ist s_m2_out
    sc_signal<T> s_r4_q;
    sc_signal<bool> s_r4_write;

    // s_r5_d ist s_a1_out
    sc_signal<T> s_r5_q;
    sc_signal<bool> s_r5_write;

    // s_r6_d ist s_m1_out
    sc_signal<T> s_r6_q;
    sc_signal<bool> s_r6_write;

    // s_r7_d ist s_m2_out
    sc_signal<T> s_r7_q;
    sc_signal<bool> s_r7_write;

    // Steuersignale für m1_in1 und m1_in2
    sc_signal<sc_uint<2> > s_m1_in1_sel;
    sc_signal<sc_uint<2> > s_m1_in2_sel;

    // Steuersignale für m2_in1 und m2_in2
    sc_signal<sc_uint<1> > s_m2_in1_sel;
    sc_signal<sc_uint<1> > s_m2_in2_sel;

    // Steuersignale für a1_in1 und a1_in2
    sc_signal<sc_uint<2> > s_a1_in1_sel;
    sc_signal<sc_uint<2> > s_a1_in2_sel;

    // Steuersignale für rx_d, ry_d, ru_d
    sc_signal<sc_uint<1> > s_rx_d_sel;
    sc_signal<sc_uint<1> > s_ry_d_sel;
    sc_signal<sc_uint<1> > s_ru_d_sel;

    // Konstanten -> Werden entsprechend im Konstruktor initialisiert
    sc_signal<T> const_3;
    sc_signal<T> const_0;

    sc_signal<bool> s_ready;

  public:
    EquationSolver(sc_module_name name) : sc_module(name), m1("m1"), ... a1("a1"), ... r1("r1"), ... {
        // SC_THREAD(go);
        SC_METHOD(go);
        sensitive << clock.pos();
        dont_initialize();

        /*************************************
         *   Ports mit Signalen verknüpfen   *
         *************************************/

        m1_in1.sel(..);
        m1_in1.in1(...);
        m1_in1.in2(...);
        m1_in1.in3(...);
        m1_in1.in4(...);
        m1_in1.out(s_m1_in1);

        m1_in2.sel(...);
        m1_in2.in1(...);
        m1_in2.in2(...);
        m1_in2.in3(...);
        m1_in2.in4(...);
        m1_in2.out(s_m1_in2);

        m1.in1(s_m1_in1);
        m1.in2(s_m1_in2);
        m1.out(s_m1_out);

        m2_in1.sel(s_m2_in1_sel);
        m2_in1.in1(...);
        m2_in1.in2(...);
        m2_in1.out(s_m2_in1);

        m2_in2.sel(s_m2_in2_sel);
        m2_in2.in1(...);
        m2_in2.in2(...);
        m2_in2.out(s_m2_in2);

        m2.in1(s_m2_in1);
        m2.in2(s_m2_in2);
        m2.out(s_m2_out);

        a1_in1.sel(..);
        a1_in1.in1(...);
        a1_in1.in2(...);
        a1_in1.in3(...);
        a1_in1.in4(...);
        a1_in1.out(s_a1_in1);

        a1_in2.sel(..);
        a1_in2.in1(...);
        a1_in2.in2(...);
        a1_in2.in3(...);
        a1_in2.in4(...);
        a1_in2.out(s_a1_in2);

        a1.in1(s_a1_in1);
        a1.in2(s_a1_in2);
        a1.out(s_a1_out);
        a1.op(...);
        a1.leq(...);

        a2.in1(...);
        a2.in2(...);
        a2.out(s_a2_out);
        a2.op(...);
        a2.leq(...);

        r1.d(s_m1_out);
        r1.q(s_r1_q);
        r1.write(s_r1_write);
        r1.clock(clock);

        r2.d(s_m2_out);
        r2.q(s_r2_q);
        r2.write(s_r2_write);
        r2.clock(clock);

        r3.d(...);
        r3.q(s_r3_q);
        r3.write(s_r3_write);
        r3.clock(clock);

        r4.d(...);
        r4.q(s_r4_q);
        r4.write(s_r4_write);
        r4.clock(clock);

        r5.d(...);
        r5.q(s_r5_q);
        r5.write(s_r5_write);
        r5.clock(clock);

        r6.d(...);
        r6.q(s_r6_q);
        r6.write(s_r6_write);
        r6.clock(clock);

        r7.d(...);
        r7.q(s_r7_q);
        r7.write(s_r7_write);
        r7.clock(clock);

        rx_d.sel(..);
        rx_d.in1(...);
        rx_d.in2(...);
        rx_d.out(s_rx_d);

        rxneu.d(...);
        rxneu.q(s_rxneu_q);
        rxneu.write(s_rxneu_write);
        rxneu.clock(clock);

        rx.d(s_rx_d);
        rx.q(s_rx_q);
        rx.write(s_rx_write);
        rx.clock(clock);

        ry_d.sel(..);
        ry_d.in1(...);
        ry_d.in2(...);
        ry_d.out(s_ry_d);

        ry.d(...);
        ry.q(s_ry_q);
        ry.write(s_ry_write);
        ry.clock(clock);

        ru_d.sel(..);
        ru_d.in1(...);
        ru_d.in2(...);
        ru_d.out(s_ru_d);

        ru.d(...);
        ru.q(s_ru_q);
        ru.write(s_ru_write);
        ru.clock(clock);

        rc.d(...);
        rc.q(s_rc_q);
        rc.write(s_rc_write);
        rc.clock(clock);

        // Konstante Signalwerte initialisieren
        const_0 = 0;
        const_3 = 3;

        // Initialzustand
        state = WAIT_START_HIGH;

        // Trace-Datei öffnen
        tf = sc_create_vcd_trace_file("simulation");

        /*************************************
         *    Signale zu Trace hinzufügen    *
         *************************************/
        sc_trace(tf, clock, "clock");
        sc_trace(tf, s_rc_q, "s_rc_q");
        sc_trace(tf, s_ru_q, "s_ru_q");
        sc_trace(tf, s_ry_q, "s_ry_q");
        sc_trace(tf, s_rx_q, "s_rx_q");
        sc_trace(tf, s_r1_q, "s_r1_q");
        sc_trace(tf, s_r2_q, "s_r2_q");
        sc_trace(tf, s_r3_q, "s_r3_q");
        sc_trace(tf, s_r4_q, "s_r4_q");
        sc_trace(tf, s_r5_q, "s_r5_q");
        sc_trace(tf, s_r6_q, "s_r6_q");
        sc_trace(tf, s_r7_q, "s_r7_q");

        sc_trace(tf, s_rc_d, "s_rc_d");
        sc_trace(tf, s_ru_d, "s_ru_d");
        sc_trace(tf, s_ry_d, "s_ry_d");
        sc_trace(tf, s_a1_out, "s_rx_d");

        sc_trace(tf, s_m1_in1, "s_m1_in1");
        sc_trace(tf, s_m1_in2, "s_m1_in2");
        sc_trace(tf, s_m1_out, "s_m1_out");

        sc_trace(tf, s_m2_in1, "s_m2_in1");
        sc_trace(tf, s_m2_in2, "s_m2_in2");
        sc_trace(tf, s_m2_out, "s_m2_out");

        sc_trace(tf, s_a1_in1, "s_a1_in1");
        sc_trace(tf, s_a1_in2, "s_a1_in2");
        sc_trace(tf, s_a1_out, "s_a1_out");
        sc_trace(tf, s_a1_op, "s_a1_op");
        sc_trace(tf, s_ry_q, "s_a2_in1");
        sc_trace(tf, s_r7_q, "s_a2_in2");
        sc_trace(tf, s_a2_out, "s_a2_out");
        sc_trace(tf, s_a2_op, "s_a2_op");

        sc_trace(tf, s_rx_write, "s_rx_write");
        sc_trace(tf, s_ry_write, "s_ry_write");
        sc_trace(tf, s_ru_write, "s_ru_write");
        sc_trace(tf, s_rc_write, "s_rc_write");
        sc_trace(tf, s_r1_write, "s_r1_write");
        sc_trace(tf, s_r2_write, "s_r2_write");
        sc_trace(tf, s_r3_write, "s_r3_write");
        sc_trace(tf, s_r4_write, "s_r4_write");
        sc_trace(tf, s_r5_write, "s_r5_write");
        sc_trace(tf, s_r6_write, "s_r6_write");
        sc_trace(tf, s_r7_write, "s_r7_write");
        sc_trace(tf, s_a1_in1_sel, "s_a1_in1_sel");
        sc_trace(tf, s_a1_in2_sel, "s_a1_in2_sel");
        sc_trace(tf, s_m1_in1_sel, "s_m1_in1_sel");
        sc_trace(tf, s_m1_in2_sel, "s_m1_in2_sel");
        sc_trace(tf, s_m2_in1_sel, "s_m2_in1_sel");
        sc_trace(tf, s_m2_in2_sel, "s_m2_in2_sel");
        sc_trace(tf, s_ry_d_sel, "s_ry_d_sel");
        sc_trace(tf, s_ru_d_sel, "s_ru_d_sel");

        sc_trace(tf, start, "start");
        sc_trace(tf, state, "state");
    }

    ~EquationSolver() {
        // Trace-Datei schließen
        sc_close_vcd_trace_file(tf);
    }

  private:
    SC_HAS_PROCESS(EquationSolver);
    sc_trace_file* tf;

    /***************************************************
     * Zustände (ergänzen, falls neue benötigt werden) *
     ***************************************************/
    enum states { WAIT_START_HIGH, WAIT_START_LOW, T_0, T_1, T_2, T_3, T_4, T_5, T_6 };
    sc_signal<int> state;

    void go() {
        switch (state) {
            case WAIT_START_HIGH:
                // Registerinitialwerte setzen

                /****************************************
                 * Steuersignale für Multiplexer setzen *
                 ****************************************/

                // x0_in, y0_in und u0_in müssen gespeichert werden
                s_rx_d_sel = 0;
                s_ry_d_sel = 0;
                s_ru_d_sel = 0;

                /*********************************************
                 * write-Signale für Register setzen/löschen *
                 *********************************************/

                s_rxneu_write = false;
                s_rx_write = true;
                s_ry_write = true;
                s_ru_write = true;
                s_rc_write = false;
                s_r1_write = false;
                s_r2_write = false;
                s_r3_write = false;
                s_r4_write = false;
                s_r5_write = false;
                s_r6_write = false;
                s_r7_write = false;
                // Warte auf positives Startsignal
                if (start) {
                    state = T_0;
                    ready = false;
                }
                break;
            case T_0:
                // t = 0

                /*********************************************
                 *   Steuersignale für Multiplexer setzen    *
                 *********************************************/
                s_m1_in1_sel = ...;
                s_m1_in2_sel = ...;
                s_m2_in1_sel = ...;
                s_m2_in2_sel = ...;
                s_a1_in1_sel = ...;
                s_a1_in2_sel = ...;

                s_rx_d_sel = ...;
                s_ry_d_sel = ...;
                s_ru_d_sel = ...;

                /*********************************************
                 *        Auswahl der ALU-Operationen        *
                 *********************************************/

                s_a1_op = ...(ADD oder SUB oder CMP);
                s_a2_op = ...;

                /*********************************************
                 * write-Signale für Register setzen/löschen *
                 *********************************************/

                s_rxneu_write = ...;
                s_rx_write = ...;
                s_ry_write = ...;
                s_ru_write = ...;
                s_rc_write = ...;
                s_r1_write = ...;
                s_r2_write = ...;
                s_r3_write = ...;
                s_r4_write = ...;
                s_r5_write = ...;
                s_r6_write = ...;
                s_r7_write = ...;

                state = T_1;
                break;
            case T_1:
                // t = 1

                /*********************************************
                 *   Steuersignale für Multiplexer setzen    *
                 *********************************************/
                s_m1_in1_sel = ...;
                s_m1_in2_sel = ...;
                s_m2_in1_sel = ...;
                s_m2_in2_sel = ...;
                s_a1_in1_sel = ...;
                s_a1_in2_sel = ...;

                s_rx_d_sel = ...;
                s_ry_d_sel = ...;
                s_ru_d_sel = ...;

                /*********************************************
                 *        Auswahl der ALU-Operationen        *
                 *********************************************/

                s_a1_op = ...(ADD oder SUB oder CMP);
                s_a2_op = ...;

                /*********************************************
                 * write-Signale für Register setzen/löschen *
                 *********************************************/

                s_rxneu_write = ...;
                s_rx_write = ...;
                s_ry_write = ...;
                s_ru_write = ...;
                s_rc_write = ...;
                s_r1_write = ...;
                s_r2_write = ...;
                s_r3_write = ...;
                s_r4_write = ...;
                s_r5_write = ...;
                s_r6_write = ...;
                s_r7_write = ...;

                state = T_2;
                break;

            case T_2:
                // t = 2

                /*********************************************
                 *   Steuersignale für Multiplexer setzen    *
                 *********************************************/
                s_m1_in1_sel = ...;
                s_m1_in2_sel = ...;
                s_m2_in1_sel = ...;
                s_m2_in2_sel = ...;
                s_a1_in1_sel = ...;
                s_a1_in2_sel = ...;

                s_rx_d_sel = ...;
                s_ry_d_sel = ...;
                s_ru_d_sel = ...;

                /*********************************************
                 *        Auswahl der ALU-Operationen        *
                 *********************************************/

                s_a1_op = ...(ADD oder SUB oder CMP);
                s_a2_op = ...;
                ;
                /*********************************************
                 * write-Signale für Register setzen/löschen *
                 *********************************************/

                s_rxneu_write = ...;
                s_rx_write = ...;
                s_ry_write = ...;
                s_ru_write = ...;
                s_rc_write = ...;
                s_r1_write = ...;
                s_r2_write = ...;
                s_r3_write = ...;
                s_r4_write = ...;
                s_r5_write = ...;
                s_r6_write = ...;
                s_r7_write = ...;

                state = T_3;
                break;

            case T_3:
                // t = 3

                /*********************************************
                 *   Steuersignale für Multiplexer setzen    *
                 *********************************************/
                s_m1_in1_sel = ...;
                s_m1_in2_sel = ...;
                s_m2_in1_sel = ...;
                s_m2_in2_sel = ...;
                s_a1_in1_sel = ...;
                s_a1_in2_sel = ...;

                s_rx_d_sel = ...;
                s_ry_d_sel = ...;
                s_ru_d_sel = ...;

                /*********************************************
                 *        Auswahl der ALU-Operationen        *
                 *********************************************/

                s_a1_op = ...(ADD oder SUB oder CMP);
                s_a2_op = ...;

                /*********************************************
                 * write-Signale für Register setzen/löschen *
                 *********************************************/

                s_rxneu_write = ...;
                s_rx_write = ...;
                s_ry_write = ...;
                s_ru_write = ...;
                s_rc_write = ...;
                s_r1_write = ...;
                s_r2_write = ...;
                s_r3_write = ...;
                s_r4_write = ...;
                s_r5_write = ...;
                s_r6_write = ...;
                s_r7_write = ...;

                state = T_4;
                break;

            case T_4:
                // t = 4

                /*********************************************
                 *   Steuersignale für Multiplexer setzen    *
                 *********************************************/
                s_m1_in1_sel = ...;
                s_m1_in2_sel = ...;
                s_m2_in1_sel = ...;
                s_m2_in2_sel = ...;
                s_a1_in1_sel = ...;
                s_a1_in2_sel = ...;

                s_rx_d_sel = ...;
                s_ry_d_sel = ...;
                s_ru_d_sel = ...;

                /*********************************************
                 *        Auswahl der ALU-Operationen        *
                 *********************************************/

                s_a1_op = ...(ADD oder SUB oder CMP);
                s_a2_op = ...;

                /*********************************************
                 * write-Signale für Register setzen/löschen *
                 *********************************************/

                s_rxneu_write = ...;
                s_rx_write = ...;
                s_ry_write = ...;
                s_ru_write = ...;
                s_rc_write = ...;
                s_r1_write = ...;
                s_r2_write = ...;
                s_r3_write = ...;
                s_r4_write = ...;
                s_r5_write = ...;
                s_r6_write = ...;
                s_r7_write = ...;

                state = T_5;
                break;

            case T_5:
                // t = 5

                /*********************************************
                 *   Steuersignale für Multiplexer setzen    *
                 *********************************************/
                s_m1_in1_sel = ...;
                s_m1_in2_sel = ...;
                s_m2_in1_sel = ...;
                s_m2_in2_sel = ...;
                s_a1_in1_sel = ...;
                s_a1_in2_sel = ...;

                s_rx_d_sel = ...;
                s_ry_d_sel = ...;
                s_ru_d_sel = ...;

                /*********************************************
                 *        Auswahl der ALU-Operationen        *
                 *********************************************/

                s_a1_op = ...(ADD oder SUB oder CMP);
                s_a2_op = ...;

                /*********************************************
                 * write-Signale für Register setzen/löschen *
                 *********************************************/

                s_rxneu_write = ...;
                s_rx_write = ...;
                s_ry_write = ...;
                s_ru_write = ...;
                s_rc_write = ...;
                s_r1_write = ...;
                s_r2_write = ...;
                s_r3_write = ...;
                s_r4_write = ...;
                s_r5_write = ...;
                s_r6_write = ...;
                s_r7_write = ...;

                state = T_6;
                break;

            case T_6:
                // t = 6

                /*********************************************
                 *   Steuersignale für Multiplexer setzen    *
                 *********************************************/
                s_m1_in1_sel = ...;
                s_m1_in2_sel = ...;
                s_m2_in1_sel = ...;
                s_m2_in2_sel = ...;
                s_a1_in1_sel = ...;
                s_a1_in2_sel = ...;

                s_rx_d_sel = ...;
                s_ry_d_sel = ...;
                s_ru_d_sel = ...;

                /*********************************************
                 *        Auswahl der ALU-Operationen        *
                 *********************************************/

                s_a1_op = ...(ADD oder SUB oder CMP);
                s_a2_op = ...;

                /*********************************************
                 * write-Signale für Register setzen/löschen *
                 *********************************************/

                s_rxneu_write = ...;
                s_rx_write = ...;
                s_ry_write = ...;
                s_ru_write = ...;
                s_rc_write = ...;
                s_r1_write = ...;
                s_r2_write = ...;
                s_r3_write = ...;
                s_r4_write = ...;
                s_r5_write = ...;
                s_r6_write = ...;
                s_r7_write = ...;

                /*********************************************
                 * Ausgänge Schreiben / Sprung für Iteration *
                 *********************************************/

                y_out.write(ry.q.read());

                if (...) {
                    state = T_0;
                } else {
                    state = WAIT_START_LOW;
                    ready = true;
                }
                break;

            case WAIT_START_LOW:

                s_rxneu_write = false;
                s_rx_write = false;
                s_ry_write = false;
                s_ru_write = false;
                s_rc_write = false;
                s_r1_write = false;
                s_r2_write = false;
                s_r3_write = false;
                s_r4_write = false;
                s_r5_write = false;
                s_r6_write = false;
                s_r7_write = false;

                // Warte auf negatives Startsignal
                if (!start) {
                    state = WAIT_START_HIGH;
                    ready = false;
                }
                break;

            default:
                // Ungültiger Zustand
                assert(0);
        }
    }
};