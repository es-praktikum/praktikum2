#pragma once

#define SC_INCLUDE_FX
#include <systemc.h>

template <class T>
class MUL : public sc_module {
  public:
    // Erster Operand
    sc_in<T> in1;
    // Zweiter Operand
    sc_in<T> in2;
    // Ergebnis
    sc_out<T> out;

    MUL(sc_module_name name) : sc_module(name) {
        SC_METHOD(go);
        sensitive << in1 << in2;

        SC_METHOD(present);
        sensitive << delay;
        dont_initialize();
    }

  private:
    SC_HAS_PROCESS(MUL);
    sc_event delay;

    void go() {
        // std::cout << "MUL::go()" << std::endl;
        delay.cancel();
        delay.notify(sc_time(18, SC_NS));
    }

    void present() {
        // std::cout << "MUL::present()" << std::endl;
        out = in1 * in2;
    }
};