#include "EquationSolverTop.hpp"

#define SC_INCLUDE_FX
#include <systemc.h>

#include <iostream>

int sc_main(int, char**) {
    /*********************************************************
     * Instantiiere das Top-Modul mit dem gegebenen Datentyp *
     *********************************************************/
    EquationSolverTop<double> top("top", "simulation.data");

    // Startet die Simulation
    sc_start(5000000, SC_NS);
    return 0;
}
