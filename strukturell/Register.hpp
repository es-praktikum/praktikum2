#pragma once

#define SC_INCLUDE_FX
#include <systemc.h>

template <class T>
class Register : public sc_module {
  public:
    // Dateneingang
    sc_in<T> d;
    // Gespeicherter Wert
    sc_out<T> q;
    // d wird bei pos. Taktflanke übernommen, falls write = 1
    sc_in<bool> write;
    // Clock
    sc_in<bool> clock;

    Register(sc_module_name name) : sc_module(name) {
        SC_METHOD(go);
        dont_initialize();
        sensitive << clock.pos();
    }

  private:
    SC_HAS_PROCESS(Register);

    void go() {
        // std::cout << "Register::go()" << std::endl;
        if (write) q = d;
    }
};