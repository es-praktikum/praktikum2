#pragma once

#define SC_INCLUDE_FX
#include <systemc.h>

template <class T>
class Multiplexer2 : public sc_module {
  public:
    // Eingang 1
    sc_in<T> in1;
    // Eingang 2
    sc_in<T> in2;
    // Daten vom ausgewählten Eingang
    sc_out<T> out;
    // Steuersignal
    sc_in<sc_uint<1> > sel;

    Multiplexer2(sc_module_name name) : sc_module(name) {
        SC_METHOD(go);
        sensitive << sel << in1 << in2;

        SC_METHOD(present);
        sensitive << delay;
        dont_initialize();
    }

  private:
    SC_HAS_PROCESS(Multiplexer2);
    sc_event delay;

    void go() {
        // std::cout << "Multiplexer2::go()" << std::endl;
        delay.cancel();
        delay.notify(sc_time(1, SC_NS));
    }

    void present() {
        // std::cout << "Multiplexer2::present()" << std::endl;
        switch (sel.read()) {
            case 0:
                out = in1;
                break;
            case 1:
                out = in2;
                break;
            default:
                assert(!"Invalid selection value!");
        }
    };
};

template <class T>
class Multiplexer4 : public sc_module {
  public:
    sc_in<T> in1;
    sc_in<T> in2;
    sc_in<T> in3;
    sc_in<T> in4;
    sc_out<T> out;
    sc_in<sc_uint<2> > sel;

    Multiplexer4(sc_module_name name) : sc_module(name) {
        SC_METHOD(go);
        sensitive << sel << in1 << in2 << in3 << in4;

        SC_METHOD(present);
        sensitive << delay;
        dont_initialize();
    }

  private:
    SC_HAS_PROCESS(Multiplexer4);
    sc_event delay;

    void go() {
        // std::cout << "Multiplexer4::go()" << std::endl;
        delay.cancel();
        delay.notify(sc_time(1, SC_NS));
    }

    void present() {
        // std::cout << "Multiplexer4::present()" << std::endl;
        switch (sel.read()) {
            case 0:
                out = in1;
                break;
            case 1:
                out = in2;
                break;
            case 2:
                out = in3;
                break;
            case 3:
                out = in4;
                break;
            default:
                assert(!"Invalid selection value!");
        }
    };
};