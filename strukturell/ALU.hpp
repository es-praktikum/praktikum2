#pragma once

#define SC_INCLUDE_FX
#include <systemc.h>

template <class T>
class ALU : public sc_module {
  public:
    // Erster Operand
    sc_in<T> in1;
    // Zweiter Operand
    sc_in<T> in2;
    // Ergebnis
    sc_out<T> out;
    // Zu berechnende Operation
    sc_in<sc_uint<2> > op;
    // True, falls in1 <= in2, sonst false
    sc_out<bool> leq;

    // Erlaubte Operationen
    enum Op {
        ADD = 0,
        SUB = 1,
        CMP = 2,
    };

    ALU(sc_module_name name) : sc_module(name) {
        SC_METHOD(go);
        sensitive << in1 << in2 << op;

        SC_METHOD(present);
        sensitive << delay;
        dont_initialize();
    }

  private:
    SC_HAS_PROCESS(ALU);
    sc_event delay;

    void go() {
        // std::cout << "ALU::go()" << std::endl;
        delay.cancel();
        delay.notify(sc_time(8, SC_NS));
    }

    void present() {
        // std::cout << "ALU::present()" << std::endl;
        leq = in1 <= in2;
        switch (op.read()) {
            case ADD:
                out = in1 + in2;
                break;
            case CMP:
            case SUB:
                out = in1 - in2;
                break;
            default:
                assert(!"Unknow operation");
        }
    }
};